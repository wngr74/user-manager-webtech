FROM node:12

WORKDIR /usr/src/app
COPY package.json ./
RUN npm install
COPY ./src .

EXPOSE 5000

CMD ["npm","run","prod"]