import { Request, Response, NextFunction, Router } from 'express'
import { UserSchema, User } from '../../../models/User';
import { v4 as uuidv4 } from 'uuid'

// TODO: Replace with DB Access
let users: User[] = []
// -----

const router: Router = Router()

const validatePayload = (req: Request, res: Response, next: NextFunction) => {
    if(UserSchema.validate(req.body).error) {
        res.status(422).end()
        return
    }
    next()
}

router.get('/', (req: Request, res: Response) => {
    res.status(200).json(users)
})

router.get('/:userId', (req: Request, res: Response) => {
    const user: User | undefined = users.find((user) => user.id === req.params.userId)
    if(user) {
        res.status(200).json(user)
    } else {
        res.status(404).end()
    }
})

router.post('/', validatePayload, (req: Request, res: Response) => {
    const newId = uuidv4()
    const newUser: User = User.fromJSON({id: newId, ...req.body})
    console.log(newUser)
    if(users.some((user) => user.id === newUser.id)) {
        res.status(422).end()
    } else {
        users.push(newUser);
        res.status(201).json(newUser)
    }
})

router.put('/:userId', validatePayload, (req: Request, res: Response) => {
    const newUser: User = User.fromJSON(req.body)
    const index: number = users.findIndex((user) => user.id === newUser.id)
    if (index != -1) {
        users[index] = newUser;
        res.status(200).json(newUser)
    } else {
        users.push(newUser)
        res.status(201).json(newUser)
    }
})

router.delete('/:userId', (req: Request, res: Response) => {
    const newUser: User = User.fromJSON(req.body)
    users = users.filter(user => user.id === newUser.id)
    res.status(200).end()
})

router.all('/', (req: Request, res: Response) => {
    res.status(405).send()
})

export default router