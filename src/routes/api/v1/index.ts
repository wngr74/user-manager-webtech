import express from 'express'
import userRouter from './users';

const router: express.Router = express.Router()

router.use('/users', userRouter)
// router.use('/client', express.static(__dirname + '/../../../../client/src'))
router.use('/client', express.static('./client'))

export default router